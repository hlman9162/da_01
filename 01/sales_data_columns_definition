-- Column descriptions

-- Sale information
SALE_ID: Unique identifier for each sale
DATE_: Date of the sale
TIME_: Time of the sale
WEEK: Week number of the sale
MONTH_: Month number of the sale
QUARTER: Quarter of the sale
YEAR_: Year of the sale

-- Customer information
CUSTOMER_ID: Unique identifier for each customer
CUSTOMER_NAME: Name of the customer
CUSTOMER_EMAIL: Email address of the customer
CUSTOMER_PHONE: Phone number of the customer
CUSTOMER_ADDRESS: Address of the customer
CUSTOMER_CITY: City of the customer
CUSTOMER_STATE: State of the customer
CUSTOMER_ZIP: ZIP code of the customer
CUSTOMER_COUNTRY: Country of the customer

-- Product information
PRODUCT_ID: Unique identifier for each product
PRODUCT_NAME: Name of the product
PRODUCT_DESCRIPTION: Description of the product
PRODUCT_CATEGORY: Category of the product
PRODUCT_SUBCATEGORY: Subcategory of the product
PRODUCT_BRAND: Brand of the product
PRODUCT_CODE: Product code of the product
PRODUCT_WEIGHT: Weight of the product
PRODUCT_HEIGHT: Height of the product
PRODUCT_WIDTH: Width of the product
PRODUCT_DEPTH: Depth of the product

-- Sales information
UNIT_PRICE: Price of a single unit of the product
QUANTITY: Quantity of the product sold
DISCOUNT: Discount given on the sale
DISCOUNT_CODE: Discount code used on the sale
TAX: Tax applied on the sale
SHIPPING_COST: Shipping cost of the sale
TOTAL_PRICE: Total price of the sale

-- Sales representative information
SALES_REP_ID: Unique identifier for each sales representative
SALES_REP_NAME: Name of the sales representative
SALES_REP_EMAIL: Email address of the sales representative
SALES_REP_PHONE: Phone number of the sales representative
SALES_REP_REGION: Region of the sales representative

-- Sales channel information
SALES_CHANNEL: Channel through which the sale was made

-- Payment information
PAYMENT_METHOD: Method of payment for the sale
PAYMENT_STATUS: Status of the payment for the sale
PAYMENT_DATE: Date of the payment for the sale

-- Shipping information
SHIPPING_DATE: Date of shipping for the sale
SHIPPING_METHOD: Method of shipping for the sale
SHIPPING_COMPANY: Company used for shipping
TRACKING_NUMBER: Tracking number for the shipment
DELIVERY_DATE: Date of delivery for the shipment
DELIVERY_TIME: Time of delivery for the shipment
DELIVERY_STATUS: Status of the delivery for the shipment

-- Return information
RETURN_DATE: Date of the return
RETURN_REASON: Reason for the return
REFUND_AMOUNT: Amount refunded for the return

-- Invoice information
INVOICE_ID: Unique identifier for each invoice
INVOICE_DATE: Date of the invoice
INVOICE_TOTAL: Total amount on the invoice
PAYMENT_DUE_DATE: Due date for the payment on the invoice
PAYMENT_AMOUNT: Amount paid on the invoice
PAYMENT_DATE_ON_INVOICE: Date of payment on the invoice

-- Shipping and billing addresses
SHIPPING_ADDRESS: Address for shipping
SHIPPING_CITY: City for shipping
SHIPPING_STATE: State for shipping
SHIPPING_ZIP: ZIP code for shipping
SHIPPING_COUNTRY: Country for shipping
BILLING_ADDRESS: Address for billing
BILLING_CITY: City for billing
BILLING_STATE: State for billing
BILLING_ZIP: ZIP code for billing
BILLING_COUNTRY: Country for billing

-- Other information
NOTES: Any additional notes
IP_ADDRESS: IP address of the customer
GENDER: Gender of the customer
AGE: Age of the customer
MARITAL_STATUS: Marital status of the customer
OCCUPATION: Occupation of the customer
EDUCATION: Education level of the customer
INCOME_LEVEL: Income level of the customer

-- Source information
SOURCE_CHANNEL: Channel through which the customer found the website
SOURCE_MEDIUM: Medium through which the customer found the website
SOURCE_CAMPAIGN: Campaign through which
