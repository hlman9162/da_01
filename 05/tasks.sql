--Find the total sales amount for a specific date range:

SELECT SUM(TOTAL_PRICE) AS total_sales
FROM sales_data
WHERE DATE_ BETWEEN TO_DATE('2023-01-01', 'YYYY-MM-DD') AND TO_DATE('2023-01-31', 'YYYY-MM-DD');


--Calculate the average sales amount for each sales representative, only considering sales above a certain threshold:
SELECT SALES_REP_ID, AVG(TOTAL_PRICE) AS average_sales
FROM sales_data
WHERE TOTAL_PRICE > 1000
GROUP BY SALES_REP_ID;


--Find the sales representative who made the highest sale in the previous month:
SELECT SALES_REP_ID, MAX(TOTAL_PRICE) AS highest_sale
FROM sales_data
WHERE EXTRACT(MONTH FROM DATE_) = EXTRACT(MONTH FROM ADD_MONTHS(SYSDATE, -1))
AND EXTRACT(YEAR FROM DATE_) = EXTRACT(YEAR FROM ADD_MONTHS(SYSDATE, -1));

--List the customers who have made purchases above the company-wide average:
SELECT DISTINCT CUSTOMER_ID
FROM sales_data
WHERE TOTAL_PRICE > (SELECT AVG(TOTAL_PRICE) FROM sales_data);


--Determine the month with the highest total sales:
SELECT MONTH_, SUM(TOTAL_PRICE) AS total_sales
FROM sales_data
GROUP BY MONTH_
HAVING SUM(TOTAL_PRICE) = (
  SELECT MAX(total_monthly_sales)
  FROM (
    SELECT MONTH_, SUM(TOTAL_PRICE) AS total_monthly_sales
    FROM sales_data
    GROUP BY MONTH_
  )
);


--Find the top 3 sales representatives by total sales for a specific year and quarter, along with their total sales amount:
SELECT *
FROM (
  SELECT SALES_REP_ID, SUM(TOTAL_PRICE) AS total_sales
  FROM sales_data
  WHERE YEAR_ = 2023 AND QUARTER = 1
  GROUP BY SALES_REP_ID
  ORDER BY total_sales DESC
)
WHERE ROWNUM <= 3;


--Calculate the year-over-year growth percentage for each product category:

WITH previous_year_sales AS (
  SELECT PRODUCT_CATEGORY, SUM(TOTAL_PRICE) AS sales
  FROM sales_data
  WHERE YEAR_ = EXTRACT(YEAR FROM SYSDATE) - 1
  GROUP BY PRODUCT_CATEGORY
),
current_year_sales AS (
  SELECT PRODUCT_CATEGORY, SUM(TOTAL_PRICE) AS sales
  FROM sales_data
  WHERE YEAR_ = EXTRACT(YEAR FROM SYSDATE)
  GROUP BY PRODUCT_CATEGORY
)
SELECT c.PRODUCT_CATEGORY, ((c.sales - p.sales) / p.sales) * 100 AS growth_percentage
FROM current_year_sales c
JOIN previous_year_sales p ON c.PRODUCT_CATEGORY = p.PRODUCT_CATEGORY;


--Find the top 5 product categories with the highest average order value, considering only completed payments:

SELECT *
FROM (
  SELECT PRODUCT_CATEGORY, AVG(TOTAL_PRICE) AS average_order_value
  FROM sales_data
  WHERE PAYMENT_STATUS = 'Completed'
  GROUP BY PRODUCT_CATEGORY
  ORDER BY average_order_value DESC
)
WHERE ROWNUM <= 5;


--Find the total sales amount per sales channel, but only for customers in a specific age range and income level:
SELECT SALES_CHANNEL, SUM(TOTAL_PRICE) AS total_sales
FROM sales_data
WHERE AGE BETWEEN 25 AND 34 AND INCOME_LEVEL = 'High'
GROUP BY SALES_CHANNEL;


--List the top 3 countries by the total number of completed payments, where the total price of the sale is above the global average:
--sql

SELECT *
FROM (
  SELECT CUSTOMER_COUNTRY, COUNT(*) AS completed_payments
  FROM sales_data
  WHERE PAYMENT_STATUS = 'Completed' AND TOTAL_PRICE > (SELECT AVG(TOTAL_PRICE) FROM sales_data)
  GROUP BY CUSTOMER_COUNTRY
  ORDER BY completed_payments DESC
)
WHERE ROWNUM <= 3;

